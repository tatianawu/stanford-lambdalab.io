---
title: Class Information
subtitle: CS43 -  Winter Quarter 2019
---

## General Info

This course explores the philosophy and fundamentals of functional programming,
focusing on the Haskell language, its theoretical underpinnings, and practical
applications. Topics include: functional abstractions
(function composition, higher order functions), immutable data structures, type
systems, and various functional design patters (monads, etc). 

- Lectures: Mon / Wed: 4:30pm - 5:50pm, in Building 200, Room 030.
- Instructors: Adithya Ganesh and Isaac Scheinfeld
- Advisor: Jerry Cain
- Office Hours: By appointment, with scheduled times TBD.

## Resources

There is no formal textbook, but we recommend the Haskell wikibook as a primary reference.  A larger compilation of resources can be found [here](/resources.html).

## Assignments

- [Assignment 1](/assignments/assign1.html) (out: 1/14, due: 1/21). Solutions [here](/solutions/assign1.html).
- [Assignment 2](/assignments/assign2.html) (out: 1/21, due: 1/28).

## Prerequisites

There are no formal prerequisites, but we recommend "programming and mathematical maturity."  In terms of Stanford courses, CS107 + CS103 or equivalent experience will suffice.


## Expectations

To pass the class (graded C/NC), we expect:

1. Attendance in $\ge$ 80% of lectures.
2. Successful completion of $\ge 6/8$ assignments, which are designed to take around an hour each.

We also welcome open source contributions to the course materials; we will offer extra credit for this on a case-by-case basis to replace assignments. 

## Attendance form

The attendance form is [here](https://goo.gl/forms/rXTlBo5CrRSvpdxp1).

## Anonymous feedback

Any feedback is appreciated; the anonymous form is [here](https://goo.gl/forms/NX7aVMRIG072csX53).
